#include "kernel/common.cl"


////////////////////////////////////////////////////////////////////////////////
/////////////////////////////// game_of_life
////////////////////////////////////////////////////////////////////////////////

static int compute_new_state (__global unsigned *cur_img,__global unsigned *next_img, int y, int x)
{
  unsigned n      = 0;
  unsigned change = 0;

  if (x > 0 && x < DIM - 1 && y > 0 && y < DIM - 1) {
    for (int i = y - 1; i <= y + 1; i++)
      for (int j = x - 1; j <= x + 1; j++)
        if (i != y || j != x)
          n += (cur_img[i*DIM + j] != 0);

    if (cur_img[y* DIM + x] != 0) {
      if (n == 2 || n == 3)
        n = 0xFFFF00FF;
      else {
        n      = 0;
        change = 1;
      }
    } else {
      if (n == 3) {
        n      = 0xFFFF00FF;
        change = 1;
      } else
        n = 0;
    }

    next_img [y*DIM +x] = n;
  }

  return change;
}


static int voisinsChange(int y, int x, __global int *changes){

  for (int i = y - 1; i <= y + 1; i++)
    for (int j = x - 1; j <= x + 1; j++)
      if(changes[i*get_global_size(0) + j]) return 1;
  return 0;
}

// Version optimisée
__kernel void vie (__global unsigned *cur_img,
		      __global unsigned *next_img, __global int *changes, __global int *changes_next)
{
  int i = get_global_id (1);
  int j = get_global_id (0);
  int tranche = get_local_size(0);

  unsigned change = 0;
  if (! voisinsChange(i+1,j+1, changes)){
    return;
  }

  for (int y = i * tranche; y <= (i+1) * tranche -1; y++)
    for (int x = j * tranche; x <= (j+1) * tranche - 1; x++)
      change |= compute_new_state (cur_img, next_img, y, x);


  changes_next[(i + 1) * get_global_size(1) + (j + 1)] = change;


}

// Version tuilée

// __kernel void vie (__global unsigned *cur_img,
// 		      __global unsigned *next_img)
// {
//   int i = get_global_id (1) % get_local_size(1);
//   int j = get_global_id (0) % get_local_size(0);
//   int tranche = get_local_size(0);
//
//   unsigned change = 0;
//
//   for (int y = i * tranche; y <= (i+1) * tranche -1; y++)
//     for (int x = j * tranche; x <= (j+1) * tranche - 1; x++)
//       change |= compute_new_state (cur_img, next_img, y, x);
//
//
//
// }


// Kernel pour version 1/2 OMP 1/2 OCL (problèmes d'affichage)
// __kernel void vie (__global unsigned *cur_img,
// 		      __global unsigned *next_img)
// {
//   int i = get_global_id (1);
//   int j = get_global_id (0);
//   compute_new_state (cur_img, next_img, i, j);
//
//
//
// }
