
#include "compute.h"
#include "debug.h"
#include "global.h"
#include "graphics.h"
#include "ocl.h"
#include "scheduler.h"
#include "monitoring.h"

#include <stdbool.h>

int **changes;
int **changes_next;

static int compute_new_state (int y, int x)
{
  unsigned n      = 0;
  unsigned change = 0;

  if (x > 0 && x < DIM - 1 && y > 0 && y < DIM - 1) {
    for (int i = y - 1; i <= y + 1; i++)
      for (int j = x - 1; j <= x + 1; j++)
        if (i != y || j != x)
          n += (cur_img (i, j) != 0);

    if (cur_img (y, x) != 0) {
      if (n == 2 || n == 3)
        n = 0xFFFF00FF;
      else {
        n      = 0;
        change = 1;
      }
    } else {
      if (n == 3) {
        n      = 0xFFFF00FF;
        change = 1;
      } else
        n = 0;
    }

    next_img (y, x) = n;
  }

  return change;
}

static int traiter_tuile (int i_d, int j_d, int i_f, int j_f)
{
  unsigned change = 0;

  PRINT_DEBUG ('c', "tuile [%d-%d][%d-%d] traitée\n", i_d, i_f, j_d, j_f);
  for (int i = i_d; i <= i_f; i++)
    for (int j = j_d; j <= j_f; j++)
      change |= compute_new_state (i, j);

  return change;
}

// Renvoie le nombre d'itérations effectuées avant stabilisation, ou 0
unsigned vie_compute_base (unsigned nb_iter)
{
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    for (int i = 1; i < DIM-1; i++)
      for (int j = 1; j < DIM-1; j++)
        change |= compute_new_state (i, j);

    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}

// Renvoie le nombre d'itérations effectuées avant stabilisation, ou 0
unsigned vie_compute_base_omp_static (unsigned nb_iter)
{
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    #pragma omp parallel for schedule(static,DIM-1) collapse(2)
    for (int i = 1; i < DIM-1; i++)
      for (int j = 1; j < DIM-1; j++)
        change |= compute_new_state (i, j);

    swap_images ();

  /*  if (!change)
      return it;*/
  }

  return 0;
}
// Renvoie le nombre d'itérations effectuées avant stabilisation, ou 0
unsigned vie_compute_base_omp_dynamic (unsigned nb_iter)
{
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    #pragma omp parallel for schedule(dynamic) collapse(2)
    for (int i = 1; i < DIM-1; i++)
      for (int j = 1; j < DIM-1; j++)
        change |= compute_new_state (i, j);

    swap_images ();

  /*  if (!change)
      return it;*/
  }

  return 0;
}


unsigned vie_compute_tile (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    // On traite toute l'image en un coup (oui, c'est une grosse tuile)
    //unsigned change = traiter_tuile (0, 0, DIM - 1, DIM - 1);
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        change |= traiter_tuile(i * tranche /* i debut */, j * tranche /* j debut */,
                           (i + 1) * tranche - 1 /* i fin */,
                           (j + 1) * tranche - 1 /* j fin */);
         #ifdef ENABLE_MONITORING
             monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
         #endif
      }
    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}

unsigned vie_compute_tile_omp_dynamic (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    #pragma omp parallel for schedule(dynamic) collapse(2)
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        change |= traiter_tuile(i * tranche /* i debut */, j * tranche /* j debut */,
                           (i + 1) * tranche - 1 /* i fin */,
                           (j + 1) * tranche - 1 /* j fin */);
         #ifdef ENABLE_MONITORING
             monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
         #endif
      }
    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}

unsigned vie_compute_tile_omp_static (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    #pragma omp parallel for schedule(static,GRAIN) collapse(2)
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        change |= traiter_tuile(i * tranche /* i debut */, j * tranche /* j debut */,
                           (i + 1) * tranche - 1 /* i fin */,
                           (j + 1) * tranche - 1 /* j fin */);
         #ifdef ENABLE_MONITORING
             monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
         #endif
      }
    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}

unsigned vie_compute_tile_omp_task (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    #pragma omp parallel
    #pragma omp single
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        #pragma omp task
        {
        change |= traiter_tuile(i * tranche /* i debut */, j * tranche /* j debut */,
                           (i + 1) * tranche - 1 /* i fin */,
                           (j + 1) * tranche - 1 /* j fin */);
         #ifdef ENABLE_MONITORING
             monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
         #endif
       }
      }

    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}


int voisinsChange(int y, int x){

  for (int i = y - 1; i <= y + 1; i++)
    for (int j = x - 1; j <= x + 1; j++)
      if(changes[i][j]) return 1;
  return 0;
}
void printChanges(){
  printf("---------------------------------\n");
  for (size_t i = 0; i < GRAIN+2; i++) {
    for (size_t j = 0; j < GRAIN+2; j++) {
      printf("%d ", changes[i][j]);
    }
    printf("\n" );
  }
  printf("---------------------------------\n");
}

static int traiter_tuile_opti (int i_d, int j_d, int i_f, int j_f)
{
  unsigned change = 0;

  PRINT_DEBUG ('c', "tuile [%d-%d][%d-%d] traitée\n", i_d, i_f, j_d, j_f);

  int tranche = DIM/GRAIN;
  for (int i = i_d; i <= i_f; i++)
    for (int j = j_d; j <= j_f; j++)
      change |= compute_new_state (i, j);

  changes_next[i_d/tranche + 1][j_d/tranche + 1] = change;

  return change;
}

void swap_changes()
{
    for(int i = 1; i < GRAIN + 1; i++ ){
      for(int j = 1; j < GRAIN +1; j++){
        changes[i][j] = changes_next[i][j];
      }
    }
}

unsigned vie_compute_opti (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    // On traite toute l'image en un coup (oui, c'est une grosse tuile)
    //unsigned change = traiter_tuile (0, 0, DIM - 1, DIM - 1);
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        if (! voisinsChange(i+1,j+1)){
          continue;
        }
        change |= traiter_tuile_opti(i * tranche /* i debut */, j * tranche /* j debut */,
                           (i + 1) * tranche - 1 /* i fin */,
                           (j + 1) * tranche - 1 /* j fin */);
         #ifdef ENABLE_MONITORING
             monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
         #endif
      }
    swap_changes();
    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}

unsigned vie_compute_opti_omp_dynamic (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    // On traite toute l'image en un coup (oui, c'est une grosse tuile)
    //unsigned change = traiter_tuile (0, 0, DIM - 1, DIM - 1);
    #pragma omp parallel for schedule(dynamic) collapse(2)
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        if (! voisinsChange(i+1,j+1)){
          continue;
        }
        change |= traiter_tuile_opti(i * tranche /* i debut */, j * tranche /* j debut */,
                           (i + 1) * tranche - 1 /* i fin */,
                           (j + 1) * tranche - 1 /* j fin */);
         #ifdef ENABLE_MONITORING
             monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
         #endif
      }
    swap_changes();
    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}

unsigned vie_compute_opti_omp_static (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    // On traite toute l'image en un coup (oui, c'est une grosse tuile)
    //unsigned change = traiter_tuile (0, 0, DIM - 1, DIM - 1);
    #pragma omp parallel for schedule(static,GRAIN) collapse(2)
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        if (! voisinsChange(i+1,j+1)){
          continue;
        }
        change |= traiter_tuile_opti(i * tranche /* i debut */, j * tranche /* j debut */,
                           (i + 1) * tranche - 1 /* i fin */,
                           (j + 1) * tranche - 1 /* j fin */);
         #ifdef ENABLE_MONITORING
             monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
         #endif
      }
    swap_changes();
    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}
unsigned vie_compute_opti_omp_task (unsigned nb_iter)
{
  int tranche = DIM / GRAIN;
  for (unsigned it = 1; it <= nb_iter; it++) {
    unsigned change = 0;

    // On traite toute l'image en un coup (oui, c'est une grosse tuile)
    //unsigned change = traiter_tuile (0, 0, DIM - 1, DIM - 1);
    #pragma omp parallel
    #pragma omp single
    for (int i = 0; i < GRAIN; i++)
      for (int j = 0; j < GRAIN; j++)
      {
        if (! voisinsChange(i+1,j+1)){
          continue;
        }
        #pragma omp task
        {
          change |= traiter_tuile_opti(i * tranche /* i debut */, j * tranche /* j debut */,
            (i + 1) * tranche - 1 /* i fin */,
            (j + 1) * tranche - 1 /* j fin */);

          #ifdef ENABLE_MONITORING
          monitoring_add_tile (i*tranche, j*tranche, tranche, tranche, 0);
          #endif
        }
      }
    swap_changes();
    swap_images ();

    if (!change)
      return it;
  }

  return 0;
}

void vie_init(){
  changes = malloc(sizeof(int) * (GRAIN+2));
  changes_next = malloc(sizeof(int) * (GRAIN+2));

  for (size_t i = 0; i < GRAIN+2; i++) {
    changes[i] = malloc(sizeof(int) * (GRAIN+2));
    changes_next[i] = malloc(sizeof(int) * (GRAIN+2));
    for (size_t j = 0; j < GRAIN+2; j++){
      changes[i][j] = (i == 0 || j == 0 || i == GRAIN+1 || j == GRAIN+1)? 0 : 1;
      changes_next[i][j] = (i == 0 || j == 0 || i == GRAIN+1 || j == GRAIN+1)? 0 : 1;
    }
  }

}


void vie_finalize(){
  for (size_t i = 0; i < GRAIN+2; i++) {
    free(changes_next[i]);
    free(changes[i]);
  }
  free(changes_next);
  free(changes);

}

// Version Tuilée OCL

// unsigned vie_compute_ocl (unsigned nb_iter)
// {
//
//   cl_int err;
//   size_t global[2] = {DIM, DIM};   // global domain size for our calculation
//   size_t local[2]  = {DIM/GRAIN, DIM/GRAIN}; // local domain size for our calculation
//   for (unsigned it = 1; it <= nb_iter; it++) {
//
//     // Set kernel arguments
//     //
//     err = 0;
//     err |= clSetKernelArg (compute_kernel, 0, sizeof (cl_mem), &cur_buffer);
//     err |= clSetKernelArg (compute_kernel, 1, sizeof (cl_mem), &next_buffer);
//     check (err, "Failed to set kernel arguments");
//
//     err = clEnqueueNDRangeKernel (queue, compute_kernel, 2, NULL, global, local,
//                                   0, NULL, NULL);
//     check (err, "Failed to execute kernel");
//
//
//
//     // Swap buffers
//     {
//       cl_mem tmp  = cur_buffer;
//       cur_buffer  = next_buffer;
//       next_buffer = tmp;
//     }
//   }
//
//   return 0;
// }


// version optimisée OCL
unsigned vie_compute_ocl (unsigned nb_iter)
{

  cl_int err;
  size_t global[2] = {GRAIN, GRAIN};   // global domain size for our calculation
  size_t local[2]  = {DIM/GRAIN, DIM/GRAIN}; // local domain size for our calculation
  for (unsigned it = 1; it <= nb_iter; it++) {

    // Set kernel arguments
    //
    err = 0;
    err |= clSetKernelArg (compute_kernel, 0, sizeof (cl_mem), &cur_buffer);
    err |= clSetKernelArg (compute_kernel, 1, sizeof (cl_mem), &next_buffer);
    err |= clSetKernelArg (compute_kernel, 2, sizeof (cl_mem), &changes_buffer);
    err |= clSetKernelArg (compute_kernel, 3, sizeof (cl_mem), &next_changes_buffer);
    check (err, "Failed to set kernel arguments");

    err = clEnqueueNDRangeKernel (queue, compute_kernel, 2, NULL, global, local,
                                  0, NULL, NULL);
    check (err, "Failed to execute kernel");



    // Swap buffers
    {
      cl_mem tmp  = cur_buffer;
      cur_buffer  = next_buffer;
      next_buffer = tmp;

      cl_mem tmp_changes = changes_buffer;
      changes_buffer = next_changes_buffer;
      next_changes_buffer = tmp_changes;
    }
  }

  return 0;
}



// Version 1/2 OCL 1/2 OMP (problèmes d'affichage)
//
// unsigned vie_compute_ocl (unsigned nb_iter)
// {
//
//   cl_int err;
//   size_t global[2] = {DIM/2, DIM};   // global domain size for our calculation
//   size_t local[2]  = {DIM/2, DIM/2}; // local domain size for our calculation
//   for (unsigned it = 1; it <= nb_iter; it++) {
//
//     // Set kernel arguments
//     //
//     err = 0;
//     err |= clSetKernelArg (compute_kernel, 0, sizeof (cl_mem), &cur_buffer);
//     err |= clSetKernelArg (compute_kernel, 1, sizeof (cl_mem), &next_buffer);
//     check (err, "Failed to set kernel arguments");
//
//     err = clEnqueueNDRangeKernel (queue, compute_kernel, 2, NULL, global, NULL,
//                                   0, NULL, NULL);
//     check (err, "Failed to execute kernel");
//
//
//     // Swap buffers
//     {
//       cl_mem tmp  = cur_buffer;
//       cur_buffer  = next_buffer;
//       next_buffer = tmp;
//     }
//
//     ocl_retrieve_image (image);
//
//     #pragma omp parallel for schedule(static) collapse(2)
//     for (int i = 0; i <= DIM-1; i++)
//       for (int j = DIM/2; j <= DIM-1; j++)
//         compute_new_state(i, j);
//
//   }
//   swap_images();
//   ocl_send_image(image);
//
//   return 0;
// }

//
// void vie_refresh_img(){
// }

///////////////////////////// Configuration initiale

void draw_stable (void);
void draw_guns (void);
void draw_random (void);
void draw_clown (void);
void draw_diehard (void);

void vie_draw (char *param)
{
  char func_name[1024];
  void (*f) (void) = NULL;

  if (param == NULL)
    f = draw_guns;
  else {
    sprintf (func_name, "draw_%s", param);
    f = dlsym (DLSYM_FLAG, func_name);

    if (f == NULL) {
      PRINT_DEBUG ('g', "Cannot resolve draw function: %s\n", func_name);
      f = draw_guns;
    }
  }

  f ();
}

static unsigned couleur = 0xFFFF00FF; // Yellow

static void gun (int x, int y, int version)
{
  bool glider_gun[11][38] = {
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
       0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
       0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0},
      {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0,
       0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1,
       0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0,
       0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
       0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
  };

  if (version == 0)
    for (int i = 0; i < 11; i++)
      for (int j = 0; j < 38; j++)
        if (glider_gun[i][j])
          cur_img (i + x, j + y) = couleur;

  if (version == 1)
    for (int i = 0; i < 11; i++)
      for (int j = 0; j < 38; j++)
        if (glider_gun[i][j])
          cur_img (x - i, j + y) = couleur;

  if (version == 2)
    for (int i = 0; i < 11; i++)
      for (int j = 0; j < 38; j++)
        if (glider_gun[i][j])
          cur_img (x - i, y - j) = couleur;

  if (version == 3)
    for (int i = 0; i < 11; i++)
      for (int j = 0; j < 38; j++)
        if (glider_gun[i][j])
          cur_img (i + x, y - j) = couleur;
}

void draw_stable (void)
{
  for (int i = 1; i < DIM - 2; i += 4)
    for (int j = 1; j < DIM - 2; j += 4)
      cur_img (i, j) = cur_img (i, (j + 1)) = cur_img ((i + 1), j) =
          cur_img ((i + 1), (j + 1))        = couleur;
}

void draw_guns (void)
{
  memset (&cur_img (0, 0), 0, DIM * DIM * sizeof (cur_img (0, 0)));

  gun (0, 0, 0);
  gun (0, DIM - 1, 3);
  gun (DIM - 1, DIM - 1, 2);
  gun (DIM - 1, 0, 1);
}

void draw_random (void)
{
  for (int i = 1; i < DIM - 1; i++)
    for (int j = 1; j < DIM - 1; j++)
      cur_img (i, j) = random () & 01;
}

void draw_clown (void)
{
  memset (&cur_img (0, 0), 0, DIM * DIM * sizeof (cur_img (0, 0)));

  int mid                = DIM / 2;
  cur_img (mid, mid - 1) = cur_img (mid, mid) = cur_img (mid, mid + 1) =
      couleur;
  cur_img (mid + 1, mid - 1) = cur_img (mid + 1, mid + 1) = couleur;
  cur_img (mid + 2, mid - 1) = cur_img (mid + 2, mid + 1) = couleur;
}

void draw_diehard (void)
{
  memset (&cur_img (0, 0), 0, DIM * DIM * sizeof (cur_img (0, 0)));

  int mid = DIM / 2;

  cur_img (mid, mid - 3) = cur_img (mid, mid - 2) = couleur;
  cur_img (mid + 1, mid - 2)                      = couleur;

  cur_img (mid - 1, mid + 3)     = couleur;
  cur_img (mid + 1, mid + 2)     = cur_img (mid + 1, mid + 3) =
      cur_img (mid + 1, mid + 4) = couleur;
}
